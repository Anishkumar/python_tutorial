#python inbuilt functions and its use
from functools import reduce


print "enumerate"
print "========="
"""enumerate takes iteratable obj as arg and returns enum object, which contains list of tuples with values and its index."""
l1 = ['a','b','c']
obj = list(enumerate(l1))
print obj


print "\n\nmap function"
print "================"
#lambda - function without a name
#map - applies a function to all the elements in a list
l1 = [1,2,3,4] #take square of all the elements
squared = list(map(lambda x: x**2, l1))
print squared

print "\n\nfilter function"
print "=================="
#filter - creates a list for which a function returns True.
l1 = [1,2,3,4,-5,-4,-3]
less_than_zero = list(filter(lambda x: x<0, l1))
print less_than_zero

print "\n\nreduce function"
print "=================="
#reduce - performs some computation on a list and return a list.
l1 = [1,2,3,4]
product = reduce((lambda x, y: x*y),l1)
print product

print "\n\nzip function"
print "=================="
#zip function
a = [1,2,3,4,5]
b = [6,7,8,9,1]
print [x+y for x,y in zip(a,b)]

print "\n\nmap function"
print "=================="
#map function
data = raw_input("Enter Values : ").split()
print type(data)
print data[0], type(data[0])
data = map(int,data)
print data[0], type(data[0])