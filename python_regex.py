#Python regex or Regular expression helps to match text using some special sequence of characters.

import re


text = "AA BB CC DD"
pattern = r'\S{2}\s?' #\S - matches all non space char, \s matches all single space
match = re.findall(pattern,text)
print match

text1 = "<@U4T8JSRE> this is a sample text <@U4T8JSRE><@U4T8JSRE> <@U4T8JSRE>"
pattern = '<@[a-z|0-9|A-Z]*>'
find = re.findall(pattern,text1)
match = re.search(pattern,text1)
print "findall : ", find
print "search : ", match.string



text = "abc$lkjshinglskdjlse&wls"
pattern = r'\w{3}\W\w*\W\w{3}'
search = re.search(pattern,text)
print search