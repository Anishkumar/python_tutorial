

dict = {
	"key" : "value"
}	#Dictionary contains key and value pair

lst = ["value1","value2"] # array contains values of same data types where list contains different data types
tuple_var = ("apple", "banana", "cherry") #tuple immutable object, a simple () will represents tuple
set_var = set(lst) #set contains unique elements
print "Python set : ", set_var
int_var = 5 #intiger
float_var = 4.5 #float
string_var = "This is a string" # raw string
Bool_var = True #Boolean


#examples

Employee = [{
	"name" : "john",
	"Gender" : "Male",
	"age" : 30,
	"designation" : "Developer"
},{
	"name" : "Riya",
	"Gender" : "Female",
	"age" : 27,
	"designation" : "Test Engineer"
}]

print Employee
print type(Employee)
print len(Employee)
print Employee[0]
print Employee[0]['name']
Employee[0]['age'] = 29			#updating a value inside dictionary
print Employee[0]