# deepcopy vs shalo copy vs assignment operator '='
import copy

parent = [1,2,3,4,5]

#Simple assignment operation will point to new variable of existing object
child1 = parent

#will give the same result
print "value of parent: ", parent
print "value of child1: ", child1
print "id of parent: ", id(parent)
print "id of child1: ", id(child1)


#copy() creates a reference object
child2 = copy.copy(parent)
print "value of child2: ", child2
#child2 will have differnt id compared to parent & child2, since it creates a new reference object, changes made to child2 will affect parent
print "id of child2: ", id(child2)


#deepcopy() parent copy of existing object is created in new object
child3 = copy.deepcopy(parent)

#both id differs, since deepcopy creates new object in different memory space, changes to child3 will not affect parent
print "id of a: ", id(parent)
print "id of d: ", id(child3)