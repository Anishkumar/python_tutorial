""" Print '#' in a formated way that looks like below

sample
=======

N = 4

   #
  ##
 ###
####

"""

N = int(raw_input())

for i in range (N+1):
	print " "*(N-i)+i*'#'